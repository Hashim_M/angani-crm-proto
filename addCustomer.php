<?php
session_start();
if(isset($_SESSION['username'])) {
    include 'assets/header.php';
    if (isset($_POST['submit'])) {

        $name = $_POST['name'];
        $call_back_date = $_POST['call_back_date'];
        $call_back_time = $_POST['call_back_time'];
        $disposition = $_POST['disposition'];
        $take = $_POST['take'];
        $visit_date = $_POST['visit_date'];
        $visit_time = $_POST['visit_time'];
        $email = $_POST['email'];
        $details = $_POST['details'];

        //form Validation

        //Populating the DB

        try {
            require_once 'config.php';
            $sql = "INSERT INTO client (client_name, call_back_date, call_back_time, disposition, take, visit_date, visit_time, email, details)
                VALUES (?,?,?,?,?,?,?,?,?)";
            $statement = $db->prepare($sql);
            $statement->bindParam(1, $name);
            $statement->bindParam(2, $call_back_date);
            $statement->bindParam(3, $call_back_time);
            $statement->bindParam(4, $disposition);
            $statement->bindParam(5, $take);
            $statement->bindParam(6, $visit_date);
            $statement->bindParam(7, $visit_time);
            $statement->bindParam(8, $email);
            $statement->bindParam(9, $details);
            $statement->execute();
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        } catch (PDOException $e) {
            echo "<p class='bg-warning'>" . $e->getMessage() . "</p>";
        }

        echo '<div class="col-sm-offset-3 alert alert-info alert-dismissible" role="alert">';
        echo '<button type="button" class="close " data-dismiss="alert" aria-label="Close">' . '<span aria-hidden="true">&times;</span></button>';
        echo '<strong>Client Added!' . '</strong>';
        echo '</div>';
        header('location: view.php');

    }
}else {
    header('location: index.php');
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">My Angani</li>
        </ol>
</div>
<!-- /.container-fluid-->
<div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-user"></i> Add Customer</div>
        <div class="card-body">
            <form action="addCustomer.php" method="post">
                <div class="row">
                    <div class="input-group">
                        <div class="form-group col-4 col-sm-5">
                            <label for="Name">Customer's Name</label>
                            <input class="form-control" placeholder="Customer's Name" name="name" id="name" minlength="5" maxlength="100" required>
                        </div>
                    </div>
                </div>
                <hr>
                <!-- <div class="row">
                    <div class="input-group">
                        <div class="form-group col-2">
                            <p>Question 1</p>
                            <label for="">Yes</label>
                            <input type="checkbox" onclick="javascript:QuizYesNoCheck();" id="quiz1yes">
                            <label for="">No</label>
                            <input type="checkbox" onclick="javascript:QuizYesNoCheck();" id="quiz1no">
                        </div>
                        <div class="form-group col-2" id="quiz2" >
                            <p>Question 2</p>
                            <label for="">Yes</label>
                            <input type="checkbox" onclick="javascript:QuizYesNoCheck();" id="quiz2yes">
                            <label for="">No</label>
                            <input type="checkbox" onclick="javascript:QuizYesNoCheck();" id="quiz2no">
                        </div>
                        <div class="form-group col-2" id="quiz3" >
                            <p>Question 3</p>
                            <label for="">Yes</label>
                            <input type="checkbox" onclick="javascript:QuizYesNoCheck();" id="quiz3yes">
                            <label for="">No</label>
                            <input type="checkbox" onclick="javascript:QuizYesNoCheck();" id="quiz3no">
                        </div>
                        <div class="form-group col-2" id="quiz4" >
                            <p>Question 4</p>
                            <label for="">Yes</label>
                            <input type="checkbox" onclick="javascript:QuizYesNoCheck();" id="quiz4yes">
                            <label for="">No</label>
                            <input type="checkbox" onclick="javascript:QuizYesNoCheck();" id="quiz4no">
                        </div>
                        <div class="form-group col-2" id="quiz5" >
                            <p>Question 5</p>
                            <label for="">Yes</label>
                            <input type="checkbox" onclick="javascript:QuizYesNoCheck();" id="quiz5yes">
                            <label for="">No</label>
                            <input type="checkbox" onclick="javascript:QuizYesNoCheck();" id="quiz5no">
                        </div>
                        <div class="form-group col-2" id="quiz6" >
                            <p>Question 6</p>
                            <label for="">Yes</label>
                            <input type="checkbox" onclick="javascript:QuizYesNoCheck();" id="quiz6yes">
                            <label for="">No</label>
                            <input type="checkbox" onclick="javascript:QuizYesNoCheck();" id="quiz6no">
                        </div>
                        <div class="form-group col-2" id="quiz10" >
                            <p>Question 10</p>
                            <label for="">Yes</label>
                            <input type="checkbox" onclick="javascript:QuizYesNoCheck();" id="quiz10yes">
                            <label for="">No</label>
                            <input type="checkbox" onclick="javascript:QuizYesNoCheck();" id="quiz10no">
                        </div>
                        <div class="form-group col-2" id="quiz12" >
                            <p>Question 12</p>
                            <label for="">Yes</label>
                            <input type="checkbox" onclick="javascript:QuizYesNoCheck();" id="quiz12yes">
                            <label for="">No</label>
                            <input type="checkbox" onclick="javascript:QuizYesNoCheck();" id="quiz12no">
                        </div>
                    </div>
                </div> -->
                <div class="row" id="call_back" >
                    <label class="col-sm-6">If Question <b>2</b> is <b>Yes</b> or Question <b>1</b> or <b>3</b> is <b>No</b>, Set Call Back Date and Time:</label><br><br>
                    <div class="input-group">
                        <div class="form-group col-5 col-sm-5">
                            <label for="">Call Back Date</label>
                            <input class="form-control" type="date" placeholder="Date to Call Back" name="call_back_date" id="call_back_date" required >
                        </div>
                        <div class="form-group col-5 col-sm-5">
                            <label for="Time"> Call Time</label>
                            <?php
                            $start = "00:00";
                            $end = "23:30";

                            $time_start = strtotime($start);
                            $time_end = strtotime($end);
                            $time_now = $time_start;
                            echo '<select class="form-control" name="call_back_time" required >';
                            while($time_now <= $time_end){
                                echo '<option value="'.date("H:i:s",$time_now).'">'.date("H:i:s",$time_now).'</option>';
                                $time_now = strtotime('+30 minutes',$time_now);
                            }
                            echo '</select>';
                            ?>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row" id="the-disposition" >
                    <div class="input-group">
                        <div class="form-group col-5">
                            <label>If Question <b>2</b>, <b>3</b> or <b>10</b> is <b>No</b>, Select Wrong Number or Not Interested:</label><br><br>
                            <select class="form-control" name="disposition" id="disposition" >
                                <option value="None">None</option>
                                <option value="Wrong Number">Wrong Number</option>
                                <option value="Uninterested">Not Interested</option>
                            </select>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row" id="customer-take" >
                    <div class="input-group">
                        <div class="form-group col-5">
                            <label>If Questions <b>1</b>, <b>3</b>, <b>4</b> are <b>Yes</b>, Select Customer's take on Angani Services:</label><br><br>
                            <select class="form-control" name="take" id="take" >
                                <option value="Excellent">Excellent</option>
                                <option value="Good">Good</option>
                                <option value="Bad">Bad</option>
                                <option value="Poor">Poor</option>
                            </select>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row" id="business-executive" >
                    <p class="col-4">If Question <b>10</b> is <b>Yes</b>, Business Executive Visit:</p><br>
                    <div class="input-group">
                        <div class="form-group col-4">
                            <label for="">Visit Date:</label>
                            <input class="form-control" type="date" placeholder="Visit Date" name="visit_date" id="visit_date" >
                        </div>
                        <div class="form-group col-4">
                            <label for="Time">Visit Time</label>
                            <?php
                            $start = "00:00";
                            $end = "23:30";

                            $time_start = strtotime($start);
                            $time_end = strtotime($end);
                            $time_now = $time_start;
                            echo '<select class="form-control" name="visit_time" id="visit_time" >';
                            while($time_now <= $time_end){
                                echo '<option value="'.date("H:i:s",$time_now).'">'.date("H:i:s",$time_now).'</option>';
                                $time_now = strtotime('+30 minutes',$time_now);
                            }
                            echo '</select>';
                            ?>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row" id="contact" >
                    <div class="input-group">
                        <div class="form-group col-4">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" id="email" minlength="10" maxlength="255" required >
                        </div>
                    </div>
                </div>
                <div class="row" id="more-details" >
                    <div class="input-group">
                        <div class="form-group col-6">
                            <label for="details">If Question <b>12</b> is <b>Yes</b> Get Details:</label>
                            <textarea class="form-control" name="details" id="details" minlength="2" maxlength="400" ></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="input-group">
                        <div class="form-group col-4 col-sm-8">
                            <button type="submit" class="btn btn-success" name="submit"> Submit</button>
                        </div>
                    </div>
                </div>
            </form>
       </div>
</div>
<!-- /.content-wrapper-->
<?php include 'assets/footer.php'; ?>
