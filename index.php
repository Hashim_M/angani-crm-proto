<?php
session_start();
if (isset($_POST['submit'])){
    try {

        $username = strip_tags($_POST['username']);
        $password = md5(strip_tags($_POST['password']));


        include_once "config.php";
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $query = "SELECT * FROM members WHERE username = :username AND password = :password";

        $stmt = $db->prepare($query);
        $stmt->execute(
            array(':username'=> $username,
                ':password'=> $password
            )
        );
        $count = $stmt->rowCount();

        if ($count == 1 ) {
            $_SESSION['username'] = $_POST['username'];
            header("location:view.php");
        }else {
            header("location:index.php");
        }

    } catch (PDOException $e){
        echo "<p class='bg-warning'>".$e->getMessage()."</p>";
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Angani | CRM</title>
    <link href="img/favicons.png" rel="shortcut icon">
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
    <div class="row offset-4">
        <img src="img/logo.png" class="img-responsive img-rounded " alt="">
    </div>
    <div class="row offset-4">
        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
            <form id="login_validator" method="post" action="">
                <h2>Login Please</h2>
                <div class="form-group">
                    <input name="username" id="username" type="text" class="form-control" placeholder="Username" required minlength="5" maxlength="255">
                </div>
                <div class="form-group">
                    <input type="password" placeholder="Password" name="password" class="form-control" id="password" minlength="5s" maxlength="255">
                </div>
                <button class="btn btn-default" type="submit" name="submit">Login</button>
                <a href="register.php"><button class="btn btn-default" type="button">Register</button></a>
            </form>
        </div>
        <?php
        // $site = $_SERVER['SERVER_NAME'];
        //echo $site;
        if(isset($errMsg)){
            ?>
            <div class="no_res col-sm-3 alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close " data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong><?php echo $errMsg ?></strong>
            </div>
        <?php } ?>
    </div>


</div>
<script type="text/javascript" src="dist/js/jquery.js"></script>
<script type="text/javascript" src="dist/js/bootstrap.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>
<script>
    $(function () {
        $("#login_validator").validate({
            rules: {
                username: {
                    required: true,
                    username: true,
                },
                password: {
                    required: true,
                    password: true
                }
            },
            message: {
                username: {
                    required: "required",
                },
                password: {
                    required: "required",
                    password: "Please Enter a password"
                }

            }

        });
    });
</script>
</body>
</html>