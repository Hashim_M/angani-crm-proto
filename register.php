<?php
if(isset($_POST['register'])) {
    try {
        require_once "config.php";

        $username = strip_tags($_POST['username']);
        $password = md5(strip_tags($_POST['password']));
        $email = strip_tags($_POST['email']);

        $sql="INSERT INTO members (username, password, email) VALUES(:username, :password, :email)";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':username', $username);
        $stmt->bindParam(':password', $password);
        $stmt->bindParam(':email', $email);
        $stmt->execute();


        if(isset($errorInfo[2])){
            $error = $errorInfo[2];
        }

        header("location: index.php");

    } catch (Exception $e) {
        $error = $e->getMessage();
    }

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Angani | CRM</title>
    <link href="img/favicons.png" rel="shortcut icon">
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!--[if lt IE 9]>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
    <div class="row offset-4">
        <a href="index.php"><img src="img/logo.png" class="img-responsive img-rounded" alt=""></a>
    </div>
    <div class="row offset-4 ">
        <div class="col-xs-12 col-sm-8 col-md-6">
            <form id="register_form" role="form" method="post" action="register.php">
                <h2>Register Please</h2>
                <div class="form-group">
                    <input name="username" id="username" type="text" class="form-control" placeholder="Username">
                </div>
                <div class="form-group">
                    <input name="email" id="email" class="form-control" placeholder="Email" type="email">
                </div>
                <div class="form-group">
                    <input type="password" placeholder="Password" name="password" class="form-control" id="password">
                </div>
                <div class="form-group">
                    <input name="Pass_conf" id="Pass_conf" type="password" class="form-control" placeholder="Confirm Password">
                </div>
                <button class="btn btn-default" type="submit" name="register">Submit</button>
                <a href="index.php"> <button class="btn btn-default" type="button">Back</button></a>
            </form>
        </div>
    </div>
    <script type="text/javascript" src="dist/js/jquery.js"></script>
    <script type="text/javascript" src="dist/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>
    <script>
        $(function () {
            $("#login_validator").validate({

            });
            $("#register_form").validate({
                rules: {
                    username: {
                        required: true,
                        username: true
                    },
                    email : {
                        required: true,
                        email : true
                    },
                    password: "required",
                    Pass_conf: {
                        required: true,
                        equalTo: "#password"
                    }
                },
                message: {
                    email: {
                        required: "Please enter an email",
                        email: "Enter a valid email"
                    },
                    password: {
                        required: "required"
                    }
                }
            });

        });
    </script>
</body>
</html>