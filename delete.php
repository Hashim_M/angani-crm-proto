<?php
session_start();
if(isset($_SESSION['username'])) {
    if (isset($_GET['id'])) {
        try {
            $id = $_GET['id'];
            require_once "config.php";
            $sql = "DELETE FROM client WHERE id = :id";
            $result = $db->prepare($sql);
            $result->bindValue(":id", $id);
            $result->execute();
            header("location: view.php");
            if (isset($errorInfo[2])) {
                $error = $errorInfo[2];
            }

        } catch (Exception $e) {
            $error = $e->getMessage();
        }
    }
}else {
    header('location: index.php');
}