<?php
try {
    $dsn = "mysql:host=localhost;dbname=angani";
    $user = 'root';
    $pwd = '';
    $db = new PDO($dsn, $user, $pwd);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo '<p class="bg-warning">'.$e->getMessage(); '</p>';
}