<?php include 'assets/header.php' ?>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">My Angani</li>
      </ol>
    </div>
    <!-- /.container-fluid-->
      <div class="card mb-3">
          <div class="card-header">
              <i class="fa fa-users"></i> Customers List</div>
          <div class="card-body">
              <div class="table-responsive">
                  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                      <thead>
                      <tr>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Customer's Take</th>
                          <th>Call Back Date</th>
                          <th>Call Back Time</th>
                          <th>Visit Date</th>
                          <th>Visit Time</th>
                          <th>Disposition</th>
                          <th>Details</th>
                          <th></th>
                          <th></th>
                      </tr>
                      </thead>
                      <tbody>
                       <?php
                        require_once "config.php";
                        $query = "SELECT * FROM client";
                        $results = $db->query($query);
                        $row = $results->fetchAll();
                        foreach ($row as $row) { ?>
                      <tr>
                          <td><?= $row['client_name'];?></td>
                          <td><?= $row['email'];?></td>
                          <td><?= $row['take'];?></td>
                          <td><?= $row['call_back_date'];?></td>
                          <td><?= $row['call_back_time'];?></td>
                          <td><?= $row['visit_date'];?></td>
                          <td><?= $row['visit_time'];?></td>
                          <td><?= $row['disposition'];?></td>
                          <td><?= $row['details'];?></td>
                          <td><a href="edit.php?id=<?=$row['id'];?>"><span class="btn btn-success">Edit</span></a></td>
                          <td><a href="delete.php?id=<?=$row['id'];?>"><span class="btn btn-danger">Delete</span></a></td>
                      </tr>
                        <?php }?>
                      </tbody>
                  </table>
              </div>
          </div>
</div>
<!-- /.content-wrapper-->
<?php include 'assets/footer.php'; ?>