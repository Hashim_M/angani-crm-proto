<?php
session_start();
include 'assets/header.php';
if(isset($_SESSION['username'])) {
    if (isset($_POST['submit'])) {

        $id = $_GET['id'];
        $client_name = $_POST['name'];
        $call_back_date = $_POST['call_back_date'];
        $call_back_time = $_POST['call_back_time'];
        $disposition = $_POST['disposition'];
        $take = $_POST['take'];
        $visit_date = $_POST['visit_date'];
        $visit_time = $_POST['visit_time'];
        $email = $_POST['email'];
        $details = $_POST['details'];

        //form Validation

        //Populating the DB

        try {
            require_once 'config.php';
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = 'UPDATE client SET client_name = :client_name, email =:email, call_back_date = :call_back_date, 
                call_back_time = :call_back_time, disposition = :disposition, take = :take, visit_date = :visit_date, 
                visit_time = :visit_time, details = :details WHERE id = :id';
            $stmt = $db->prepare($sql);
            $stmt->execute(
                array(
                    ':client_name' => $client_name,
                    ':email' => $email,
                    ':call_back_date' => $call_back_date,
                    ':call_back_time' => $call_back_time,
                    ':disposition' => $disposition,
                    ':take' => $take,
                    ':visit_date' => $visit_date,
                    ':visit_time' => $visit_time,
                    ':details' => $details
                )
            );
            if ($stmt) {
                header('location:view.php');
            }
        } catch (PDOException $e) {
            echo "<p class='bg-warning'>" . $e->getMessage() . "</p>";
        }

        echo '<div class="col-sm-offset-3 alert alert-info alert-dismissible" role="alert">';
        echo '<button type="button" class="close " data-dismiss="alert" aria-label="Close">' . '<span aria-hidden="true">&times;</span></button>';
        echo '<strong>Client Edited!' . '</strong>';
        echo '</div>';
        header('location: view.php');

    }
    if (isset($_GET['id'])) {
        require_once 'config.php';
        $id = $_GET['id'];
        $query = 'SELECT * FROM client WHERE id = :id';
        $results = $db->prepare($query);
        $results->bindValue(':id', $id);
        $results->execute();
        $row = $results->fetch();

        $client_name = $row['client_name'];
        $call_back_date = $row['call_back_date'];
        $call_back_time = $row['call_back_time'];
        $disposition = $row['disposition'];
        $take = $row['take'];
        $visit_date = $row['visit_date'];
        $visit_time = $row['visit_time'];
        $email = $row['email'];
        $details = $row['details'];
    }

} else {
    header('location: index.php');
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">My Angani</li>
        </ol>
    </div>
    <!-- /.container-fluid-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-user"></i> Edit Customer</div>
        <div class="card-body">
            <form action="addCustomer.php" method="post">
                <div class="row">
                    <div class="input-group">
                        <div class="form-group col-4 col-sm-5">
                            <label for="Name">Customer's Name</label>
                            <input class="form-control" placeholder="Customer's Name" name="name" id="name" minlength="5" maxlength="255" value="<?=$client_name;?>" required>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <label class="col-sm-6">If Question <b>1</b> is <b>No</b>, Set Call Back Date and Time:</label><br><br>
                    <div class="input-group">
                        <div class="form-group col-5 col-sm-5">
                            <label for="">Call Back Date</label>
                            <input class="form-control" type="date" id="call_back_date" placeholder="Date to Call Back" name="call_back_date" value="<?=$call_back_date; ?>" required>
                        </div>
                        <div class="form-group col-5 col-sm-5">
                            <label for="Time"> Call Time</label>
                            <?php
                            $start = "00:00";
                            $end = "23:30";

                            $time_start = strtotime($start);
                            $time_end = strtotime($end);
                            $time_now = $time_start;
                            echo '<select class="form-control" name="call_back_time" required>';
                            echo '<option value="'.$call_back_time.'">'.$call_back_time.'</option>';
                            while($time_now <= $time_end){
                                echo '<option value="'.date("H:i:s",$time_now).'">'.date("H:i:s",$time_now).'</option>';
                                $time_now = strtotime('+30 minutes',$time_now);
                            }
                            echo '</select>';
                            ?>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="input-group">
                        <div class="form-group col-5">
                            <label>If Question <b>2</b>, <b>3</b> or <b>10</b> is <b>No</b>, Select Wrong Number or Not Interested:</label><br><br>
                            <select class="form-control" name="disposition" id="disposition">
                                <option value="<?=$disposition ;?>"><?=$disposition ;?></option>
                                <option value="None">None</option>
                                <option value="Wrong Number">Wrong Number</option>
                                <option value="Uninterested">Not Interested</option>
                            </select>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="input-group">
                        <div class="form-group col-5">
                            <label>If Questions <b>1</b>, <b>3</b>, <b>4</b> are <b>Yes</b>, Select Customer's take on Angani Services:</label><br><br>
                            <select class="form-control" name="take" id="take">
                                <option value="<?=$take ;?>"><?=$take ;?></option>
                                <option value="Excellent">Excellent</option>
                                <option value="Good">Good</option>
                                <option value="Bad">Bad</option>
                                <option value="Poor">Poor</option>
                            </select>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <p class="col-4">If Question <b>10</b> is <b>Yes</b>, Business Executive Visit:</p><br><br>
                    <div class="input-group">
                        <div class="form-group col-4">
                            <label for="">Visit Date:</label>
                            <input class="form-control" type="date" placeholder="Visit Date" name="visit_date" value="<?=$visit_date;?>" id="visit_date">
                        </div>
                        <div class="form-group col-4">
                            <label for="Time">Visit Time</label>
                            <?php
                            $start = "00:00";
                            $end = "23:30";

                            $time_start = strtotime($start);
                            $time_end = strtotime($end);
                            $time_now = $time_start;
                            echo '<select class="form-control" name="visit_time" id="visit_time">';
                            echo '<option value="'.$visit_time.'">'.$visit_time.'</option>';
                            while($time_now <= $time_end){
                                echo '<option value="'.date("H:i:s",$time_now).'">'.date("H:i:s",$time_now).'</option>';
                                $time_now = strtotime('+30 minutes',$time_now);
                            }
                            echo '</select>';
                            ?>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="input-group">
                        <div class="form-group col-4">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" id="email" value="<?=$email;?>" minlength="10" maxlength="255" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="input-group">
                        <div class="form-group col-6">
                            <label for="details">More Questions or Details by Customer:</label>
                            <textarea class="form-control" name="details" id="details" minlength="2" maxlength="400"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="input-group">
                        <div class="form-group col-4 col-sm-8">
                            <button type="submit" class="btn btn-success" name="submit"> Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /.content-wrapper-->
    <?php include 'assets/footer.php'; ?>
